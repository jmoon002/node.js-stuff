const http = require('http');
const fs = require('fs');

function getQueryString(url) {
 if (url.slice(0, 2) == '/?') {
   url = url.slice(2);
 }

 const parts = url.split('&');
 const qs = {};

 for (let i = 0; i < parts.length; i++) {
   const metaParts = parts[i].split('=');

   if (metaParts[0] == 'file' || metaParts[0] == 'content') {
     qs[metaParts[0]] = metaParts[1];
   }
 }

 return qs;
}

function requestHandler(request, response) {

 if (request.url == '/favicon.ico') {
   return;
 }

 let qs = getQueryString(request.url);

 fs.writeFileSync(qs.file, qs.content);
 response.write('I am here');
 response.end();
}

const server = http.createServer(requestHandler);

server.listen(4000);

// http://localhost:7999?file=hulkhands&content=itswhoyouareitswhoyouare

